# Auroch Créations pour Krosmaster
*Membre fondateur des KroSympas*
## Licence:
Les documents publiés dans ce dépôt sont soumis à la [Creative Commons Attribution-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/)

Merci de créditer: **Sébastien - Auroch - BOLLINGH**
![N|Solid](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
